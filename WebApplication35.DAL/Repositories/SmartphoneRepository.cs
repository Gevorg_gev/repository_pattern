﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication35.Core.Repositories;

namespace WebApplication35.DAL.Repositories
{
    public class SmartphoneRepository : SqlRepositoryBase<Smartphone> , ISmartphoneRepository
    {
        public SmartphoneRepository(SmartphoneDbContext smartphoneDbContext) : base(smartphoneDbContext) 
        {
            
        }
    }
}
