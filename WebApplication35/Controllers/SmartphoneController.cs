using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication35.Controllers;

namespace WebApplication35 
{
    [ApiController]
    [Route("[controller]")]
    public class SmartphoneController : ControllerBase
    {
        private readonly SmartphoneDbContext smartphoneDbContext; 
        public SmartphoneController(SmartphoneDbContext smartphoneDbContext) 
        {
            this.smartphoneDbContext = smartphoneDbContext;
        }

        private static readonly string[] Smartphone_Brand = new[]
        {
            "Samsung" , "Apple" , "Xiaomi" , "Huawei" , "Honor" , "Meizu" , "Alcatel" , "Nokia"
        };

        private static readonly string[] Smartphone_Model = new[]
        {
            "Galaxy A7(2017)" , "IPhone 12Pro Max" , "Xiaomi Mi Note 10 Lite" , "Huawei X5" , "Honor S7" ,
            "Meizu Mi K3" , "Alcatel OneTouch Ocean" , "Nokia Lumia 2016"
        };

        private static readonly string[] Smartphone_Color = new[]
        {
            "Black" , "Matt Black" , "Gold" , "Rose Gold" , "Space Grey"
        };

        private static readonly string[] Smartphone_Country = new[]
        {
            "USA" , "Russia" , "China" , "Finland" , "Australia"
        };

        private static readonly string[] Smartphone_Display = new[]
        {
            "IPS LCD Display" , "Amoled Display" , "OLED Display" , "SuperAMOLED Display" , "DynamicAMOLED Display"
        };

        private static readonly string[] Smartphone_Camera = new[]
        {
            "16 Mp(rear) / 16 Mp(Front)" , "108 Mp" , "42 Mp" , "12/12/16 Mp"
        };

        private static readonly string[] Smartphone_ScreenResolution = new[]
        {
            "720P (HD)" , "1080P (FHD)" , "1440P (QHD)" , "2160P (UHD)"
        };

        [HttpGet]
        public IEnumerable<Smartphone> GetSmartphones()
        {
            var rng = new Random(); 
            return Enumerable.Range(1, 9).Select(index => new Smartphone
            {
                Id = rng.Next(1, 9),
                Brand = Smartphone_Brand[rng.Next(Smartphone_Brand.Length)],
                Model = Smartphone_Model[rng.Next(Smartphone_Model.Length)],
                Color = Smartphone_Color[rng.Next(Smartphone_Color.Length)],
                Country = Smartphone_Country[rng.Next(Smartphone_Country.Length)],
                Display = Smartphone_Display[rng.Next(Smartphone_Display.Length)],
                Camera = Smartphone_Camera[rng.Next(Smartphone_Camera.Length)],
                ScreenResolution = Smartphone_ScreenResolution[rng.Next(Smartphone_ScreenResolution.Length)] 
            });
        }

        [HttpPost]
        public IActionResult AddSmartphones([FromBody] Smartphone smartphone)
        {

            smartphoneDbContext.Add(smartphone); 


            return Ok();
        }

        [HttpDelete]
        public IActionResult RemoveSmartphones([FromRoute] int id)
        {
            smartphoneDbContext.Remove(id);


            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateSmartphones([FromBody] Smartphone smartphone, [FromRoute] int id)
        {
            smartphoneDbContext.Update(smartphone);  


            return Ok();
        } 

    }
}