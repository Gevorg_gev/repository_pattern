﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication35.Core.Abstractions
{
    public interface ISmartphoneBL
    {
        IEnumerable<Smartphone> GetSmartphones();
        bool RemoveSmartphones(int id);
        bool UpdateSmartphones(Smartphone smartphone, int id);
        Smartphone AddSmartphones(Smartphone smartphone);
    }
}
