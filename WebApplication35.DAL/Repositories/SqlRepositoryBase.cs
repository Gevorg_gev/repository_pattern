﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication35.Core.Repositories;

namespace WebApplication35.DAL.Repositories
{
    public class SqlRepositoryBase<T> : ISqlRepository<T> where T : class 
    {

        private readonly SmartphoneDbContext smartphoneDbContext;
        public SqlRepositoryBase(SmartphoneDbContext smartphoneDbContext)
        {
            this.smartphoneDbContext = smartphoneDbContext; 
        }

        public T Add(T entity)
        {
            smartphoneDbContext.Set<T>().Add(entity);
            return entity;
        }

        public T Get()
        {
            throw new NotImplementedException();
        }
 

        public IEnumerable<T> GetWhere(Func<T, bool> func)
        {
            return smartphoneDbContext.Set<T>().Where(func); 
        }

        public void Remove(T entity)
        {
            smartphoneDbContext.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            smartphoneDbContext.Set<T>().Update(entity); 
        }

        public int SaveChanges()
        {
            return smartphoneDbContext.SaveChanges(); 
        }
    }
}
