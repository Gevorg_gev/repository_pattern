﻿using System;
using System.Collections.Generic;
using WebApplication35.Core.Abstractions;
using System.Linq;

namespace WebApplication35.BLL.Oparations
{
    public class SmartphoneBL : ISmartphoneBL
    {
        private readonly SmartphoneDbContext smartphoneDbContext;
        
        public Smartphone AddSmartphones(Smartphone smartphone) 
        {
            

            smartphoneDbContext.Add(smartphone);  
            smartphoneDbContext.SaveChanges();


            return smartphone; 
        }
        private static readonly string[] Smartphone_Brand = new[]
        {
            "Samsung" , "Apple" , "Xiaomi" , "Huawei" , "Honor" , "Meizu" , "Alcatel" , "Nokia"
        };

        private static readonly string[] Smartphone_Model = new[]
        {
            "Galaxy A7(2017)" , "IPhone 12Pro Max" , "Xiaomi Mi Note 10 Lite" , "Huawei X5" , "Honor S7" ,
            "Meizu Mi K3" , "Alcatel OneTouch Ocean" , "Nokia Lumia 2016"
        };

        private static readonly string[] Smartphone_Color = new[]
        {
            "Black" , "Matt Black" , "Gold" , "Rose Gold" , "Space Grey"
        };

        private static readonly string[] Smartphone_Country = new[]
        {
            "USA" , "Russia" , "China" , "Finland" , "Australia"
        };

        private static readonly string[] Smartphone_Display = new[]
        {
            "IPS LCD Display" , "Amoled Display" , "OLED Display" , "SuperAMOLED Display" , "DynamicAMOLED Display"
        };

        private static readonly string[] Smartphone_Camera = new[]
        {
            "16 Mp(rear) / 16 Mp(Front)" , "108 Mp" , "42 Mp" , "12/12/16 Mp"
        };

        private static readonly string[] Smartphone_ScreenResolution = new[]
        {
            "720P (HD)" , "1080P (FHD)" , "1440P (QHD)" , "2160P (UHD)"
        };

        public IEnumerable<Smartphone> GetSmartphones()
        {
            var rng = new Random();
            return Enumerable.Range(1, 9).Select(index => new Smartphone
            {
                Id = rng.Next(1, 9),
                Brand = Smartphone_Brand[rng.Next(Smartphone_Brand.Length)],
                Model = Smartphone_Model[rng.Next(Smartphone_Model.Length)],
                Color = Smartphone_Color[rng.Next(Smartphone_Color.Length)],
                Country = Smartphone_Country[rng.Next(Smartphone_Country.Length)],
                Display = Smartphone_Display[rng.Next(Smartphone_Display.Length)],
                Camera = Smartphone_Camera[rng.Next(Smartphone_Camera.Length)],
                ScreenResolution = Smartphone_ScreenResolution[rng.Next(Smartphone_ScreenResolution.Length)]
            });
        }

        public bool RemoveSmartphones(int id)
        {
            var rmv = smartphoneDbContext.Smartphones.Find(id);
            if (rmv == null)
            { 
                return false;
            }

            smartphoneDbContext.Smartphones.Remove(rmv); 
            smartphoneDbContext.SaveChanges();


            return true; 
        }

        public bool UpdateSmartphones(Smartphone smartphone, int id)
        {
            var smartphoneToUpdate = smartphoneDbContext.Smartphones.Find(id);
            if (smartphoneToUpdate == null)
            {
                return false; 
            }


            smartphoneToUpdate.Id = smartphone.Id;
            smartphoneToUpdate.Model = smartphone.Model;
            smartphoneToUpdate.Brand = smartphone.Brand;
            smartphoneToUpdate.Color = smartphone.Color;
            smartphoneToUpdate.Country = smartphone.Country;
            smartphoneToUpdate.Display = smartphone.Display;
            smartphoneToUpdate.Camera = smartphone.Camera;
            smartphoneToUpdate.ScreenResolution = smartphone.ScreenResolution;

            smartphoneDbContext.Smartphones.Update(smartphoneToUpdate);
            smartphoneDbContext.SaveChanges();


            return true;
        }
    }
}
